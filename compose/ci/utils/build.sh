#!/bin/sh

get_apps_branch() {
	if [ -n "${OVERRIDE_BUILD_BRANCH}" ]; then
		echo "${OVERRIDE_BUILD_BRANCH}"
	elif echo "${CI_COMMIT_TAG}" | grep -q -E '^v[0-9]+\.[0-9]+\.[0-9]+$'; then
		echo "${CI_COMMIT_TAG}" | cut -d'.' -f1 # v4
	elif echo "${CI_COMMIT_REF_NAME}" | grep -q -E '^v[0-9]+\.[0-9]+\.[0-9]+$'; then
		echo "${CI_COMMIT_REF_NAME}" | cut -d'.' -f1 # v4
	elif echo "${CI_COMMIT_REF_NAME}" | grep -q -E '^v[0-9]+(-[a-z]+)?$'; then
		echo "${CI_COMMIT_REF_NAME}" # v4-fix, v4-dev, v4
	elif [ -n "${CI_MERGE_REQUEST_TARGET_BRANCH_PROTECTED}" ] && [ "${CI_MERGE_REQUEST_TARGET_BRANCH_PROTECTED}" = "true" ]; then
		echo "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
	elif [ -n "${CI_COMMIT_REF_PROTECTED}" ] && [ "${CI_COMMIT_REF_PROTECTED}" = "true" ]; then
		echo "${CI_COMMIT_REF_NAME}"
	else
		echo "develop"
	fi
}

get_image_tag() {
	# Grab the image tag from the CI environment variables
	image_tag="$CI_COMMIT_TAG"

	# If the image tag is empty, use the short commit hash
	if [ -z "$image_tag" ]; then
		image_tag="$CI_COMMIT_SHORT_SHA"
	fi

	# Override image tag from commit message if it contains "[docker-tag=<branch>]"
	tag_from_commit_message=$(echo "$CI_COMMIT_MESSAGE" | sed -n 's/.*\[docker-tag=\(.*\)\].*/\1/p')
	if [ -n "$tag_from_commit_message" ]; then
		image_tag="$tag_from_commit_message";
	fi

	# If the image tag is still empty, use a random string
	if [ -z "$image_tag" ]; then
		echo
		image_tag="untagged.$RANDOM"
	fi

	echo "$image_tag"
}

get_image_base() {
	if [ -z "$CI_REGISTRY_IMAGE" ]; then echo "ERROR: build.sh: CI_REGISTRY_IMAGE is not set" >&2; exit 1; fi
	echo "$CI_REGISTRY_IMAGE"
}

get_image_name() {
	image_tag="${1:-$(get_image_tag)}"
	image=$(get_image_base)
	echo "$image:$image_tag"
}

login() {
	# https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html#use-gitlab-cicd-to-authenticate
	if [ -z "$CI_REGISTRY_USER" ]; then echo "ERROR: build.sh: CI_REGISTRY_USER is not set" >&2; exit 1; fi
	if [ -z "$CI_REGISTRY_PASSWORD" ]; then echo "ERROR: build.sh: CI_REGISTRY_PASSWORD is not set" >&2; exit 1; fi
	if [ -z "$CI_REGISTRY" ]; then echo "ERROR: build.sh: CI_REGISTRY is not set" >&2; exit 1; fi
	docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
}

fetch_latest_release() {
	# res=$(curl --location --silent --header "JOB-TOKEN: $CI_JOB_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases/permalink/latest" || echo "")
	# tag=$(python3 -c 'import sys, json; print(json.load(sys.stdin)["tag_name"])' <<< "$res" || echo "")

	# On Alpine, use wget instead of curl, and "parse" the JSON response using grep
	res=$(wget --header="JOB-TOKEN: $CI_JOB_TOKEN" --quiet --output-document - "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases/permalink/latest" || echo "")
	tag=$(echo "$res" | grep -o -E '"tag_name":\s*"[^"]+"' | cut -d'"' -f4 || echo "")
	echo "$tag"
}

is_latest_release() {
	if [ "$#" -lt 1 ] || [ "$#" -gt 2 ]; then
		echo "Usage: is_latest_release <tag> [latest_tag]" >&2
		exit 1
	fi

	tag="$1"
	if [ -z "$tag" ]; then
		echo "is_latest_release: tag is empty" >&2
		exit 1
	fi

	# Check if the tag is a semantic version
	if ! echo "$tag" | grep -q -E '^v[0-9]+\.[0-9]+\.[0-9]+$'; then
		echo "is_latest_release: not a semantic version: $tag" >&2
		exit 1
	fi

	# Check if the tag is the latest release
	latest_tag="${2:-"$(fetch_latest_release)"}"
	if [ -z "$latest_tag" ]; then
		echo "is_latest_release: failed to fetch latest release" >&2
		exit 0
	fi

	# Compare the tags
	# https://stackoverflow.com/a/4025065/1664572
	# https://stackoverflow.com/a/66298321/66298607
	# if [ "$tag" \< "$latest_tag" ]; then
	if version_compare_lt "$tag" "$latest_tag"; then
		echo "is_latest_release: not the latest release: $tag < $latest_tag" >&2
		exit 1
	fi

	echo "is_latest_release: yes ($tag)" >&2
}

# https://stackoverflow.com/a/4024263
version_compare_lte() {
	[ "$1" = "$(printf "%s\n%s\n" "$1" "$2" | sort -V | head -n1)" ]
}
version_compare_lt() {
	[ "$1" = "$2" ] && return 1 || version_compare_lte "$1" "$2"
}

if [ "$1" = "get-image-tag" ]; then
	get_image_tag
elif [ "$1" = "get-image-name" ]; then
	get_image_name "$2"
elif [ "$1" = "login" ]; then
	login
elif [ "$1" = "is-latest-release" ]; then
	is_latest_release "$2"
elif [ "$1" = "get-branch" ]; then
	get_apps_branch
elif [ "$1" = "get-apps-branch" ]; then
	get_apps_branch
elif [ "$1" = "test-me" ]; then
	set -e
	fail() { echo "test failed: $1" >&2; exit 1; }
	is() {
		left="$(cat)"
		right="$1"
		if [ "$left" != "$right" ]; then fail "'$left' != '$right'"; fi
	}

	CI_API_V4_URL="https://gitlab.com/api/v4"
	CI_COMMIT_TAG="v1.2.3"
	CI_COMMIT_MESSAGE="commit message"
	CI_COMMIT_REF_NAME=$CI_COMMIT_TAG
	CI_COMMIT_SHORT_SHA="abcdef"
	CI_REGISTRY_IMAGE="registry.gitlab.com/username/project"
	CI_REGISTRY_PASSWORD="password"
	CI_REGISTRY_USER="username"
	CI_REGISTRY="registry.gitlab.com"
	get_image_tag | is "v1.2.3"
	get_image_name | is "registry.gitlab.com/username/project:v1.2.3"
	get_image_name "custom-tag" | is "registry.gitlab.com/username/project:custom-tag"
	(is_latest_release 2>/dev/null) && fail "is_latest_release: false if no tag (1)"
	(is_latest_release "" 2>/dev/null) && fail "is_latest_release: false if no tag (2)"
	(is_latest_release "" "v9.9.9" 2>/dev/null) && fail "is_latest_release: false if no tag (3)"
	(is_latest_release "develop" 2>/dev/null) && fail "is_latest_release: false if not semver"
	(is_latest_release "v1.2.3" "v9.9.9" 2>/dev/null) && fail "is_latest_release"
	(is_latest_release "v1.1.0" "v1.10.0" 2>/dev/null) && fail "is_latest_release"
	(is_latest_release "v1.9.0" "v1.10.0" 2>/dev/null) && fail "is_latest_release"
	(is_latest_release "v9.9.9" "v1.2.3" 2>/dev/null) || fail "is_latest_release"
	(is_latest_release "v1.10.0" "v1.1.0" 2>/dev/null) || fail "is_latest_release"
	(is_latest_release "v1.10.0" "v1.9.0" 2>/dev/null) || fail "is_latest_release"
	(is_latest_release "v1.2.3" "v1.2.3" 2>/dev/null) || fail "is_latest_release: true if equals"
	(is_latest_release "v1.2.4" "v1.2.3" 2>/dev/null) || fail "is_latest_release"

	CI_PROJECT_ID="DOES_NOT_EXIST"
	(is_latest_release "v0.0.1" 2>/dev/null) || fail "is_latest_release: true if fails to fetch"

	CI_PROJECT_ID="11368664"
	fetch_latest_release | grep -q -E '^v[0-9]+\.[0-9]+\.[0-9]+$' || fail "fetch_latest_release"

	echo "All tests passed"
else
	echo "ERROR: build.sh: invalid argument: ${1:-requires at least one argument}" >&2
	exit 1
fi
