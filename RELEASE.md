More info on [packaging.python.org/...](https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/#uploading-your-project-to-pypi)

# Tester rapidement un changement sur TestPyPI

Créer un token de test sur [test.pypi.org/manage/account/token/](https://test.pypi.org/manage/account/token/), puis l'ajouter au fichier `.pypirc` ([plus d'infos dans la documentation](https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/#create-an-account)).

```sh
vim $HOME/.pypirc
```

```sh
sed -i "s/^VERSION = .*/VERSION = \"v0.0.$(date +%Y%m%d%H%M%S)\"/g" bench/__init__.py
grep -m 1 VERSION bench/__init__.py
rm dist/*
python3 -m build
twine check dist/*
twine upload --repository testpypi dist/*
```


# Déployer sur PyPI

Créer un token de release sur [pypi.org/manage/account/token/](https://pypi.org/manage/account/token/), puis l'ajouter au fichier `.pypirc` ([plus d'infos dans la documentation](https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/#create-an-account)).
Le token de release doit idéalement être restreint au projet dokos-cli, et son nom contient idéalement : le nom de l'ordinateur, la date de mise à jour. Idéalement, le token est supprimé dès que la release est faite (ou juste après une vérification manuelle).

```sh
# Ajouter le token de release si besoin
vim $HOME/.pypirc

# Mettre à jour la version
vim bench/__init__.py

# Confirmer que la version est correcte
grep -m 1 VERSION bench/__init__.py

# Nettoyer les anciens builds
rm dist/*

# Créer le nouveau build
python3 -m build

# Vérifier le build
twine check dist/*

# Envoyer le build sur PyPI
twine upload dist/*

# Télécharger manuellement
pip3 install --upgrade dokos-cli
```
